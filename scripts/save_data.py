import argparse
import os
from urllib.request import urlopen 
import json 
import pandas as pd 
from sqlalchemy import create_engine, text
from dotenv import load_dotenv

load_dotenv()  # take environment variables from .env.
DB_FILE = os.getenv("DB_FILE")



def main():

    ######################
    # Handling arguments #
    ######################

    parser = argparse.ArgumentParser(description='Script to get data from an API and store it in a Database')
    parser.add_argument("-a", "--api", nargs=1, type=str, help="API URL")

    options = parser.parse_args()

    # store the URL
    url = options.api[0]
    
    # store the response of URL 
    response = urlopen(url) 
    
    # storing the JSON response from url 
    data_json = json.loads(response.read())

    # expand all json value into columns
    df = pd.json_normalize(data_json, max_level=1)
    # transpose the dataframe since data entries are spread as columns
    df = df.T
    # drop second column since it has similar data to first column
    df = df.drop([1], axis=1)
    df = df.reset_index()

    # split column into two columns
    df[['measured_at', 'datalogger']] = df['index'].str.split('.', expand=True)
    df["value"] = df[0]
    df = df.drop(["index", 0], axis=1)

    # save the data into sqlite database
    engine = create_engine("sqlite:///" + DB_FILE, echo=False)
    df.to_sql(name='meteo', con=engine, if_exists='replace', index=False)

    with engine.connect() as conn:
        conn.execute(text("SELECT * FROM meteo")).fetchall()


if __name__ == '__main__':

    main()
