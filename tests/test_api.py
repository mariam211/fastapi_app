import os
import requests
import unittest
import pandas as pd
from http import HTTPStatus
from dotenv import load_dotenv
from src.utils import *

load_dotenv()  # take environment variables from .env.
BASE_URL = os.getenv("BASE_URL")


class DFTests(unittest.TestCase):

    """ class for running unittests """

    def setUp(self):
        """ setUp """

        try:
            data = pd.read_csv(os.path.join(os.getenv("DATA_PATH"), "test_meteo.csv"))
        except IOError:
            print('cannot open file')

        self.fixture = data

    def tearDown(self):
        del self.fixture

    def test_dataFrame_constructed(self):
        """ Test that the dataframe is contructed"""
        data = get_data(os.path.join(os.getenv("DATA_PATH"), "test_meteo.db"))

        assert not data.empty and data.shape == self.fixture.shape

    def test_dataFrame_formatedAsExpected(self):
        """ Test that the dataframe has been correctly formated"""
        data = get_data(os.path.join(os.getenv("DATA_PATH"), "test_meteo.db"))
        data = format_data(data)

        assert data.measured_at.dtype == "datetime64[ns, UTC]" and data.value.dtype == "float64"

    def test_dataFrame_filteredAsExpected(self):
        """ Test that the dataframe is filtered correctly"""
        data = format_data(self.fixture)
        before = pd.to_datetime("2021-01-01T00:59:00.000", utc=True)
        since = pd.to_datetime("2021-01-01T00:15:00.000", utc=True)
        data = filter_data(self.fixture, datalogger="precip", before=before, since=since)
        
        s_data = data.sort_values(by="measured_at", ascending=True)
        start = s_data["measured_at"].iloc[0]
        end = s_data["measured_at"].iloc[-1]

        assert start >= since and end < before

    def test_dataFrame_aggregatedAsExpected(self):
        """ Test that the aggregation of the dataframe gives the expect result"""
        data = format_data(self.fixture)
        avg_data = groupby_span(data, Span.day, "avg")
        max_data = groupby_span(data, Span.hour, "max")
        min_data = groupby_span(data, Span.MAX, "min")

        assert set(avg_data["value"].values) == set([92.5, 0, (-2.2-2.3-2.2)/3]) and avg_data.shape[0] == 3 \
            and set(max_data["value"].values) == set([92.5, 0, -2.2]) and set(min_data["value"].values) == set([92.5, 0, -2.3])


def test_root() -> bool:
    """Tests api endpoint"""
    resp = requests.get(BASE_URL)
    resp.status_code = HTTPStatus.OK

    assert resp.json() == {"message":"Welcome to Fastapi app"}


def test_data() -> bool:
    """Tests api/data endpoint."""
    datalogger = "hum"
    resp = requests.get(BASE_URL + "/data?datalogger=" + datalogger)
    
    assert resp.status_code == HTTPStatus.OK


def test_data_fail() -> bool:
    """Tests api/data endpoint."""
    datalogger = "humm"
    resp = requests.get(BASE_URL + "/data?datalogger=" + datalogger)
    
    assert resp.status_code == HTTPStatus.UNPROCESSABLE_ENTITY


def test_data_before() -> bool:
    """Tests api/data endpoint."""
    datalogger = "hum"
    resp = requests.get(BASE_URL + "/data?datalogger=" + datalogger + "&before=")
    
    assert resp.status_code == HTTPStatus.UNPROCESSABLE_ENTITY


def test_data_since_unvalid() -> bool:
    """Tests the api/data endpoint with an unvalid since query."""
    datalogger = "hum"
    resp = requests.get(BASE_URL + "/data?datalogger=" + datalogger + "&since=2021/01/01")
    
    assert resp.status_code == HTTPStatus.UNPROCESSABLE_ENTITY


def test_data_since_valid() -> bool:
    """Tests the api/data endpoint with a valid since query."""
    datalogger = "temp"
    resp = requests.get(BASE_URL + "/data?datalogger=" + datalogger + "&since=2021-01-01T00:29:40.000Z")
    
    assert resp.status_code == HTTPStatus.OK


def test_summary_avg() -> bool:
    """Tests the api/summary endpoint."""
    datalogger = "hum"
    resp = requests.get(BASE_URL + "/summary?datalogger=" + datalogger + "_avg&span=max")
    
    assert resp.status_code == HTTPStatus.OK


def test_summary_max() -> bool:
    """Tests the summary endpoint."""
    datalogger = "temp"
    resp = requests.get(BASE_URL + "/summary?datalogger=" + datalogger + "_max&span=hour")
    
    assert resp.status_code == HTTPStatus.OK


def test_summary_sum() -> bool:
    """Tests the summary endpoint."""
    datalogger = "hum"
    resp = requests.get(BASE_URL + "/summary?datalogger=" + datalogger + "_sum&span=day")
    
    assert resp.status_code == HTTPStatus.UNPROCESSABLE_ENTITY


if __name__ == '__main__':
    unittest.main()