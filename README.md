## Setup

Inside "datalogger" directory run:

```code
## install dependencies
yarn install
## start mock server
yarn run json-server db.json
```

To install this project's dependencies run:

```code
pip install -r requirements.txt 
```

## Data Ingestion

In the root directory create a ".env" file with the following variables set:

```
DB_FILE={db file location in which fetched data will be saved}
DATA_PATH={path to project directory}/data/
BASE_URL={Fastapi server}/api
```

Run the script to fetch the data from the API and save it to the file "DB_FILE":

```code
python3 scripts/save_data.py -a {API_SERVER}/measurements
```

## Run App

To start Fastapi server run inside "src" folder:

```code
uvicorn main:app
```

## Tests

```code
pytest tests/test_api.py
```