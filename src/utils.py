import sqlite3
from enum import auto
from strenum import StrEnum
import pandas as pd
from datetime import datetime
from typing import Union


class DataLogger(StrEnum):
    temp = auto()
    hum = auto()
    precip = auto()


class DataLoggerAgg(StrEnum):
    temp = auto()
    temp_min = auto()
    temp_max = auto()
    temp_avg = auto()
    hum = auto()
    hum_min = auto()
    hum_max = auto()
    hum_avg = auto()
    precip = auto()
    precip_sum = auto()


class Span(StrEnum):
    raw = auto()
    MAX = 'max'
    day = auto()
    hour = auto()


def get_data(path: str) -> pd.DataFrame:
    """ Function to retrieve data from database """
    try:
        connection = sqlite3.connect(path, check_same_thread=False)
        df = pd.read_sql_query("SELECT * FROM meteo", connection, index_col=None)
    except Exception as e:
        print(e)

    return df


def format_data(df: pd.DataFrame) -> pd.DataFrame:
    """ Function to format dataframe """    
    try:
        df["measured_at"] = pd.to_numeric(df["measured_at"])
        df["measured_at"] = pd.to_datetime(df["measured_at"], unit="ms", origin="unix", utc=True )
        df["value"] = pd.to_numeric(df["value"])
    except Exception as e:
        print(e)

    return df


def filter_data(df: pd.DataFrame, datalogger: str, before: datetime, since: Union[datetime, None]=None) -> pd.DataFrame:
    """ Function for filtering a dataframe """
    try:
        df = df[df["datalogger"] == datalogger]
        df = df[df["measured_at"] < before]

        if since:
            df = df[df["measured_at"] >= since]
    except Exception as e:
        print(e)
    
    return df


def apply_aggregation(group: pd.DataFrame, agg: str) -> pd.DataFrame:
    """ Function that applies different aggregation functions on a dataframe """
    try:
        if agg == "avg":
            df = group.mean().reset_index()
        elif agg == "max":
            df = group.max().reset_index()
        elif agg == "min":
            df = group.min().reset_index()
        elif agg == "sum":
            df = group.sum().reset_index()
    except Exception as e:
        print(e)
    
    return df


def groupby_span(df: pd.DataFrame, span: datetime, agg: str) -> pd.DataFrame:
    """ Applies aggregation function over a data group specified by span """
    try:
        # group entries on the same day
        if span == Span.day:
            group = df.groupby(by=[df["measured_at"].dt.date, "datalogger"])["value"]
            df = apply_aggregation(group, agg)
        # group entries on the same hour over all the data
        elif span == Span.hour:
            group = df.groupby(by=[df["measured_at"].dt.hour, "datalogger"])["value"]
            df = apply_aggregation(group, agg)
        # group entries with same datalogger
        elif span == Span.MAX:
            start_time = df.sort_values(by="measured_at", ascending=True)["measured_at"].iloc[0]
            group = df.groupby(by=["datalogger"])["value"]
            df = apply_aggregation(group, agg)
            # set time_slot to first date of the dataframe
            df["time_slot"] = start_time
    except Exception as e:
        print(e)

    return df
