import json
import os
from typing import Union
from fastapi import FastAPI
import pandas as pd
from datetime import datetime
from utils import *
from dotenv import load_dotenv

load_dotenv()  # take environment variables from .env.
DB_FILE = os.getenv("DB_FILE")

app = FastAPI()


@app.get("/api")
def root():
    """API Endpoint."""
    return {"message": "Welcome to Fastapi app"}


@app.get("/api/data")
def read_data(datalogger: DataLogger, before: datetime = pd.Timestamp.utcnow(), since: Union[datetime, None] = None):
    """Endpoint to returns the data stored. The output is the raw data stored."""
    # get and tranform data
    df = get_data(DB_FILE)
    df = format_data(df)
    df = filter_data(df, datalogger, before, since)
    df = df.rename(columns={"datalogger": "label"})
    
    # tranform dataframe to json
    response = df.to_json(orient='records', date_format='iso')
    result = json.loads(response)

    return result


@app.get("/api/summary")
def read_summary(datalogger: DataLoggerAgg, before: datetime = pd.Timestamp.utcnow(), span: Span = Span.raw, since: Union[datetime, None] = None):
    """ 
        Endpoint to return the data stored.
        The output will be either raw data or aggregates. The behaviour is driven by the query parameter span.
    """
    datalogger_id = datalogger
    agg = None

    if "_" in datalogger:
        datalogger_id, agg = datalogger.split("_")
    
    # get and tranform data
    df = get_data(DB_FILE)
    df = format_data(df)
    df = filter_data(df, datalogger_id, before, since)

    # need span and aggregation function to apply
    if agg and span != Span.raw:
        df = groupby_span(df, span, agg)
    
    df = df.rename(columns={"datalogger": "label", "measured_at": "time_slot"})
    
    # tranform dataframe to json
    response = df.to_json(orient='records', date_format='iso')
    result = json.loads(response)

    return result
